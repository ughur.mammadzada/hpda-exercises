#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

// Function to compute the mean from a CSV file
double computeMeanCSV(const string& file) {
    ifstream data(file);

    double total = 0.0;
    int count = 0;

    string line;
    while (getline(data, line)) {
        istringstream iss(line);
        string token;
        vector<string> columns;

        while (getline(iss, token, ',')) {
            columns.push_back(token);
        }

        if (columns.size() > 1 && columns[1] != "") {
            double x = stod(columns[1]);
            total += x;
            count++;
        }
    }

    if (count > 0) {
        double mean = total / count;
        return mean;
    } else {
        return 0.0;
    }
}

int main(int argc, char** argv) {
    if (argc != 2) {
        cerr << "Usage: " << argv[0] << " [FILE]" << endl;
        return 1;
    }

    string file = argv[1];
    if (file.empty()) {
        cerr << "Use $ ./csv-cpp [FILE]" << endl;
        return 1;
    }

    double mean = computeMeanCSV(file);
    cout << mean << endl;

    return 0;
}

