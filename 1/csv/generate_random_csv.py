import pandas as pd
import random

chunk_size = 100000
chunk_amount = 1000000

file_path = "csv.csv"

df = pd.DataFrame(columns=["Column1", "Column2"])

for i in range(1, chunk_size):
    chunk_data = [[i*j, random.randint(80, 1000)] for j in range(1, chunk_size)]
    df = pd.DataFrame(chunk_data, columns=["Column1", "Column2"])
    df.to_csv(file_path, mode='a', header=False, index=False)

print('DONE')
