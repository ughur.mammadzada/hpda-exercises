import sys

def computeMeanCSV(file):
    with open(file, 'r') as data:
        total = 0
        count = 0

        for row in data:
            columns = row.strip().split(',')

            if (len(columns) > 1 and columns[1] != ''):
                x = float(columns[1])
                total = total + x
                count = count + 1

        if count > 0:
            mean = total / count
            return mean

file = sys.argv[1]
if file == '':
    print('Use $ python csv.py [FILE]')
else:
    mean = computeMeanCSV(file)
    print(mean)
