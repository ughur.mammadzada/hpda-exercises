import sys
import string
import matplotlib.pyplot as plt

file = sys.argv[1]
search_word = None
if(len(sys.argv)>2):
    search_word = ' ' + sys.argv[2] + ' '

data = None
chapters = []

divider = '\nCHAPTER'

def import_file (file):
    with open(file, 'r') as text:
        return text.read()

def divide_text (data, divider):
    text_chunks = data.split(divider)

    for i in range(1, len(text_chunks)):
        text_chunks[i] = f"{divider}{text_chunks[i]}"

    return text_chunks

def count_words (data, word):
    counts = {}

    for chunk in data:
        counts[chunk.splitlines()[1].strip()] = chunk.translate(str.maketrans('', '', string.punctuation)).count(word)

    return counts

def plot(data, y_label, x_label):
    plt.bar(data.keys(), data.values(), 0.35)
    plt.ylabel(y_label)
    plt.xlabel(x_label)
    plt.xticks(list(data.keys()), rotation='vertical')
    plt.grid(axis='y', linestyle='--', color='gray', alpha=0.6)
    plt.show()

if __name__ == '__main__':
    data = divide_text(import_file(file), divider)
    output = count_words(data, search_word)
    plot(output, 'Selected Word', 'Occurance')

