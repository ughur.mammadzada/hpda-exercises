import sys
import json

file = sys.argv[1]

def open_json(path):
    with open(path) as json_file:
        return json.load(json_file)


def calculate_total_frequencies(data):
    counts = {}
    for word, chapters in data.items():
        if (word not in counts):
            counts[word] = 0

        for frequency in chapters.values():
            counts[word] += frequency

    return counts

def get_most_frequent(data):

    copy_data = data
    top10_words = {}
    while (len(top10_words) < 9):
        most_frequent_word = max(copy_data, key=copy_data.get)
        top10_words[most_frequent_word] = copy_data[most_frequent_word]
        del copy_data[most_frequent_word]

    return top10_words

if __name__ == '__main__':
    data = open_json(file)
    frequency_of_words = calculate_total_frequencies(data)
    most_frequent_words = get_most_frequent(frequency_of_words)

    print(most_frequent_words)
