import sys
import string
import pandas as pd
import json

from graph import import_file
from graph import divide_text

file = sys.argv[1]
divider = '\nCHAPTER'

def count_unique_words(data):
    rows = []
    for chunk in data:
        chunk_words = chunk.translate(str.maketrans('', '', string.punctuation)).split()
        rows.append([
            chunk.splitlines()[1].strip(),
            len(chunk_words),
            len(set(chunk_words))
        ])

    return rows

def count_words_per_chapter(data):
    words = {}

    for chunk in data:
        chunk_head = chunk.splitlines()[1].strip()
        chunk_words = chunk.translate(str.maketrans('', '', string.punctuation)).split()

        for word in chunk_words:
            if(word not in words):
                words[word] = {}

            if (chunk_head in words[word]):
                words[word][chunk_head] += 1
            else:
                words[word][chunk_head] = 1

    return words


if __name__ == '__main__':
    data = divide_text(import_file(file), divider)

    unique_words = count_unique_words(data)
    unique_words = pd.DataFrame(unique_words, columns=['chapter-name', 'chapter-lenngth', 'unique-words'])
    unique_words.to_csv('unique_words.csv', index=False)

    words_per_chapters = count_words_per_chapter(data)
    with open('words_per_chapters.json', 'w') as json_save:
        json.dump(words_per_chapters, json_save, indent=4)

