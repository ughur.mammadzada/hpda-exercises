from data_flow import DataFlow

test = (
        DataFlow()
        .read('input.csv')
        .map(lambda x: (x[0], x[3]))
        .filter(lambda x: '16' in x[1])
        .flatmap(lambda x: [[x[0], y] for y in x[1][1:-1].strip().split()])
        .group(lambda x: x[1])
        .reduce(lambda x: len(x[1]))
        )

print(test.data)

#test = DataFlow()
#print(test.data)

#test = test.read('input.csv')
#print(test.data)

#test = test.map(lambda x: (x[0], x[3]))
#print(test.data)

#test = test.filter(lambda x: '16' in x[1])
#print(test.data)

#test = test.flatmap(lambda x: [[x[0], y] for y in x[1][1:-1].strip().split()])
#print(test.data)

#test = test.group(lambda x: x[1])
#print(test.data)

#test = test.reduce(lambda x: len(x[1]))
#print(test.data)

def join_fc(x, y):
    if x!=y:
        return [x, y]
    else:
        None

test = test.join(test, join_fc)
print(test.data)
