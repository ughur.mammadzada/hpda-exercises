import csv

class DataFlow:
    def __init__(self, data=[]):
        self.data = data

    def read(self, path):
        tmp_data = []
        with open(path, 'r') as file:
            csv_reader = csv.reader(file)
            next(csv_reader, None)

            for row in csv_reader:
                tmp_data.append(row)

        return DataFlow(tmp_data)

    def write(self, path):
        with open(path, 'w') as file:
            csv_writer = csv.writer(file)
            csv_writer.writerows(self.data)

        return DataFlow(self.data)

    def filter(self, filter_function):
        tmp_data = []

        for item in self.data:
            if filter_function(item):
                tmp_data.append(item)

        return DataFlow(tmp_data)

    def map(self, map_function):
        tmp_data = []

        for item in self.data:
            tmp_data.append(map_function(item))

        return DataFlow(tmp_data)

    def flatmap(self, flatmap_function):
        tmp_data = []

        for item in self.data:
            tmp_rows = flatmap_function(item)
            for row in tmp_rows:
                tmp_data.append(row)

        return DataFlow(tmp_data)

    def group(self, group_function):
        tmp_data = {}

        for item in self.data:
            key = group_function(item)

            if key in tmp_data:
                tmp_data[key].append(item)
            else:
                tmp_data[key] = [item]

        tmp_data = list(tmp_data.items())
        return DataFlow(tmp_data)

    def reduce(self, reduce_function):
        tmp_data = []

        for item in self.data:
            tmp_data.append(reduce_function(item))

        return DataFlow(tmp_data)

    def join(self, df, join_function):
        tmp_data = []

        for item1 in self.data:
            for item2 in df.data:
                temp_item = join_function(item1, item2)
                tmp_data.append(temp_item)

        return DataFlow(tmp_data)
