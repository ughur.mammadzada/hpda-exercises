-- User table
CREATE TABLE `Users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(39) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;

-- Location dimension
CREATE TABLE `Location` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Country` varchar(30) DEFAULT NULL,
  `City` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


-- Browser's User Agent dimension

CREATE TABLE `Browser` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_agent` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


-- Access Dates dimension

CREATE TABLE `AccessDate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestampt` timestamp NOT NULL,
  `year` smallint(5) unsigned NOT NULL,
  `month` varchar(3) NOT NULL,
  `day` tinyint(3) unsigned NOT NULL,
  `hour` tinyint(3) unsigned NOT NULL,
  `second` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `AccessDate_CHECK_DAY` CHECK (`day` between 1 and 31),
  CONSTRAINT `AccessDate_CHECK_HOUR` CHECK (`hour` between 0 and 23),
  CONSTRAINT `AccessDate_CHECK_SECOND` CHECK (`second` between 0 and 59),
  CONSTRAINT `AccessDate_CHECK_MONTH` CHECK (`month` in ('JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC'))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


-- Main table

CREATE TABLE `olap` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_id` varchar(39) NOT NULL,
  `location_id` int(10) unsigned NOT NULL,
  `acces_date_id` int(10) unsigned NOT NULL,
  `browser` int(10) unsigned NOT NULL,
  `time_spent` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`ip`),
  KEY `olap_FK` (`location_id`),
  KEY `olap_FK_1` (`borwser`),
  KEY `olap_FK_2` (`acces_date_id`),
  CONSTRAINT `olap_FK` FOREIGN KEY (`location_id`) REFERENCES `Location` (`id`),
  CONSTRAINT `olap_FK_1` FOREIGN KEY (`borwser`) REFERENCES `Browser` (`id`),
  CONSTRAINT `olap_FK_2` FOREIGN KEY (`acces_date_id`) REFERENCES `AccessDate` (`id`),
  CONSTRAINT `olap_FK_3` FOREIGN KEY (`ip_id`) REFERENCES `Users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_general_ci;


-- Insert data:
INSERT INTO AccessDate (`timestampt`, `year`, `month`, `day`, `hour`, `minute`, `second`)
VALUES (FROM_UNIXTIME(1699556421), 2023, 'NOV', 9, 7, 0, 21);

INSERT INTO AccessDate (`timestampt`, `year`, `month`, `day`, `hour`, `minute`, `second`)
VALUES (FROM_UNIXTIME(1699557114), 2023, 'NOV', 9, 7, 11, 54);

INSERT INTO AccessDate (`timestampt`, `year`, `month`, `day`, `hour`, `minute`, `second`)
VALUES (FROM_UNIXTIME(1696878741), 2023, 'OCT', 9, 7, 21, 12);

INSERT INTO Browser (`user_agent`)
VALUES ('Mozilla');

INSERT INTO Browser (`user_agent`)
VALUES ('Chrome');

INSERT INTO Location (`Country`, `City`)
VALUES ('Ireland', 'Dublin');

INSERT INTO Location (`Country`, `City`)
VALUES ('Germany', 'Berlin');

INSERT INTO Location (`Country`, `City`)
VALUES ('Ireland', 'Belfast');

INSERT INTO Users (`ip`)
VALUES ('2001:0db8:85a3:0000:0000:8a2e:0370:7334');

INSERT INTO Users (`ip`)
VALUES ('fe80::1');

INSERT INTO Users (`ip`)
VALUES ('2a00:1450:4007:809::200e');

INSERT INTO Users (`ip`)
VALUES ('170.164.290.23');

INSERT INTO olap (`ip_id`, `location_id`, `access_date_id`, `browser`, `time_spent`)
VALUES (1, 1, 1, 2, 1920);

INSERT INTO olap (`ip_id`, `location_id`, `access_date_id`, `browser`, `time_spent`)
VALUES (3, 3, 1, 2, 2093);

INSERT INTO olap (`ip_id`, `location_id`, `access_date_id`, `browser`, `time_spent`)
VALUES (2, 2, 2, 1, 4389);

INSERT INTO olap (`ip_id`, `location_id`, `access_date_id`, `browser`, `time_spent`)
VALUES (4, 3, 3, 1, 7087);


-- Calculating:

SELECT 'NOV' AS 'Month', L.Country AS 'Country', SUM(O.time_spent)/60/60 as 'Hours Spent'
FROM olap AS O
INNER JOIN AccessDate AD ON (O.acces_date_id = AD.id)
INNER JOIN Location L ON (O.location_id = L.id)
WHERE AD.month = 'Nov' AND L.Country = 'Ireland';

