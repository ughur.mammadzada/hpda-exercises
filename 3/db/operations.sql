-- MySQL / MariaDB

-- Looks for matches of given strings.
-- Used AND, to be able to seach out of order (TEST 2, 2 TEST, 2nd TEST).

SELECT *
FROM gaug_hpda.Articles
WHERE LOWER(title) LIKE CONCAT('%', '2', '%')
AND LOWER(title) LIKE CONCAT('%', 'test', '%');

-- Find rows in related table, where article_id is given id and find articles,
-- id of which is in the result of the subquery.

SELECT *
FROM gaug_hpda.Articles
WHERE id IN (
				SELECT related_id
				FROM gaug_hpda.related
				WHERE article_id = 1
			);


-- Shows articles, category id of which match the give category id.

SELECT *
FROM gaug_hpda.Articles
WHERE category = 1;


-- Replaces all substrings, which match the give word with epty stings, to find the difference.
-- After that divides the differece by the length of the given word, to find number of occurences.

SELECT
    id,
    title,
    'test' as word,
    (CHAR_LENGTH(text)-CHAR_LENGTH(REPLACE(LOWER(text), 'test', '')))/CHAR_LENGTH('test') as occurances
FROM gaug_hpda.Articles;
