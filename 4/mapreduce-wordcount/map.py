import sys
import string
from nltk.stem.snowball import SnowballStemmer
from nltk.stem import WordNetLemmatizer

stemmer = SnowballStemmer('english')
lemmatizer = WordNetLemmatizer()

file = sys.argv[1]

def import_file(file):
    with open(file, 'r') as content:
        return content.read()

def merge_lines(text):
    res = ''
    for line in text.splitlines():
        res += line + ' '

    return res.strip()

def my_map(text):
    words_stemmed = list(map(stemmer.stem, text))
    words_lemmed = list(map(lemmatizer.lemmatize, text))
    return words_stemmed, words_lemmed

if __name__ == '__main__':
    data = import_file(file)
    data = data.lower()
    data = data.translate(str.maketrans('', '', string.punctuation))
    data = merge_lines(data)

    stemmed_words, lemmed_words = my_map(data.split())

    for word in lemmed_words:
        print('L,'+word)

    for word in stemmed_words:
        print('S,'+word)

