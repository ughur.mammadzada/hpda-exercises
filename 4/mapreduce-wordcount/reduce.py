import sys
import string
from functools import reduce

input_string = sys.stdin

def unpack_input(string_input):
    input_lines = []

    for line in string_input:
        input_lines.append(line.strip())

    return input_lines


def my_reduce(occurance, item):
    word = item
    occurance[word] = occurance.get(word, 0) + 1
    return occurance

if __name__ == '__main__':
    words = unpack_input(input_string)
    result = reduce(my_reduce, words, {})

    for key, value in sorted(result.items()):
        print(key+','+str(value))
